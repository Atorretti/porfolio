import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class VanGogh implements KeyboardHandler {

    private Keyboard keyboard;

    private int cellsize = 25;
    private Rectangle vanGogh;
    private MakeGrid grid;

    public VanGogh() {

        keyboard = new Keyboard(this);

        vanGogh = new Rectangle(10, 10, 25, 25);
        vanGogh.setColor(Color.BLUE);
        vanGogh.fill();

        init();

    }


    public void init() {

        keyboard = new Keyboard(this);
        //Right key
        KeyboardEvent rightPress = new KeyboardEvent();
        rightPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        rightPress.setKey(KeyboardEvent.KEY_RIGHT);

        KeyboardEvent leftPress = new KeyboardEvent();
        leftPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        leftPress.setKey(KeyboardEvent.KEY_LEFT);

        KeyboardEvent upPress = new KeyboardEvent();
        upPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        upPress.setKey(KeyboardEvent.KEY_UP);

        KeyboardEvent downPress = new KeyboardEvent();
        downPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        downPress.setKey(KeyboardEvent.KEY_DOWN);

        KeyboardEvent paintPress = new KeyboardEvent();
        paintPress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        paintPress.setKey(KeyboardEvent.KEY_P);

        KeyboardEvent erasePress = new KeyboardEvent();
        erasePress.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        erasePress.setKey(KeyboardEvent.KEY_O);

        KeyboardEvent paintPress2 = new KeyboardEvent();
        paintPress2.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        paintPress2.setKey(KeyboardEvent.KEY_I);


        keyboard.addEventListener(rightPress);
        keyboard.addEventListener(leftPress);
        keyboard.addEventListener(upPress);
        keyboard.addEventListener(downPress);
        keyboard.addEventListener(paintPress);
        keyboard.addEventListener(erasePress);
        keyboard.addEventListener(paintPress2);


    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_RIGHT:
                System.out.println("moving right");
                if (vanGogh.getX() < (500 - cellsize)) {
                    vanGogh.translate(25, 0);
                }
                break;

            case KeyboardEvent.KEY_LEFT:
                System.out.println("moving left");
                if (vanGogh.getX() > (0 + cellsize)) {
                    vanGogh.translate(-25, 0);
                }

                break;

            case KeyboardEvent.KEY_UP:
                System.out.println("moving up");
                if (vanGogh.getY() > (0 + cellsize)) {
                    vanGogh.translate(0, -25);
                }

                break;

            case KeyboardEvent.KEY_DOWN:
                System.out.println("moving down");
                if (vanGogh.getY() < (500 - cellsize)) {
                    vanGogh.translate(0, 25);
                }
                break;

            case KeyboardEvent.KEY_P:
                System.out.println("PAINTTTTT");
                paint();
                paint1();

                break;

            case KeyboardEvent.KEY_O:
                System.out.println("ERASE");
                erase();
                erase1();

                break;

            case KeyboardEvent.KEY_I:
                System.out.println("PAINT GREEN");
                paint2();
                paint1();

                break;

        }
    }


    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }



   /* public void getVanGoghPos (){
        vanGogh.getX();
        vanGogh.getY();
        vanGogh.getWidth();
        vanGogh.getHeight();

    }*/

    public void paint() {
        Rectangle vanPainter = new Rectangle(vanGogh.getX(), vanGogh.getY(), vanGogh.getWidth(), vanGogh.getHeight());
        vanPainter.setColor(Color.ORANGE);
        vanPainter.fill();
    }

    public void paint1() {
        Rectangle vanPaint1 = new Rectangle(vanGogh.getX(), vanGogh.getY(), vanGogh.getWidth(), vanGogh.getHeight());
        // vanPaint1.setColor(Color.ORANGE);
        vanPaint1.setColor(Color.BLACK);
        vanPaint1.draw();
    }

    public void paint2() {
        Rectangle vanPainter2 = new Rectangle(vanGogh.getX(), vanGogh.getY(), vanGogh.getWidth(), vanGogh.getHeight());
        vanPainter2.setColor(Color.GREEN);
        vanPainter2.fill();
    }

    public void erase() {
        Rectangle vanErase = new Rectangle(vanGogh.getX(), vanGogh.getY(), vanGogh.getWidth(), vanGogh.getHeight());
        vanErase.setColor(Color.WHITE);
        vanErase.fill();

    }

    public void erase1() {
        Rectangle vanErase1 = new Rectangle(vanGogh.getX(), vanGogh.getY(), vanGogh.getWidth(), vanGogh.getHeight());
        //vanErase1.setColor(Color.WHITE);
        vanErase1.setColor(Color.BLACK);
        vanErase1.draw();
    }

}